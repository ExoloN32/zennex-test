import React, {useState} from 'react';
import {View, Text, TextInput, Pressable, ScrollView} from 'react-native';
import {styles} from './styles';
import {useDispatch} from 'react-redux';
import {todoActions} from '../store/todo/actions';
import ImpotantModal from './ImpotantModal';
import ExecutionTime from './ExecutionTime';
import {titleAlert} from '../utils/titleAlert';
import {timeAlert} from '../utils/timeAlert';

const CreateScreen = ({navigation}) => {
    const dispatch = useDispatch();
    const [title, setTitle] = useState('');
    const [description, setDescription] = useState('');
    const [impotant, setImpotant] = useState('обычная');
    const [modalVisible, setModalVisible] = useState(false);
    const [toggle, setToggle] = useState(false);
    const [date, setDate] = useState(new Date(0));

    const handleImpotant = value => {
        setImpotant(value);
        setModalVisible(false);
    };

    const handleReturn = () => {
        navigation.goBack();
    };

    const handleSubmit = () => {
        if (title.trim().length < 1) {
            titleAlert();
            return;
        }
        if (toggle && new Date().getTime() > new Date(date).getTime()) {
            timeAlert();
            return;
        }
        dispatch(
            todoActions.addTodo({
                id: new Date().getTime(),
                title,
                description,
                impotant,
                useDate: toggle,
                date: Date.parse(date),
                complited: false,
                completionTime: new Date().getTime(),
            }),
        );
        navigation.goBack();
    };

    return (
        <View style={styles.container}>
            <ImpotantModal
                setImpotant={handleImpotant}
                setVisible={setModalVisible}
                visible={modalVisible}>
                {['обычная', 'важная', 'очень важная']}
            </ImpotantModal>
            <ScrollView>
                <TextInput
                    style={styles.input}
                    onChangeText={setTitle}
                    value={title}
                    placeholder="Название"
                    maxLength={50}
                />
                <TextInput
                    style={styles.input}
                    onChangeText={setDescription}
                    value={description}
                    placeholder="Описание"
                    multiline
                />
                <View>
                    <Text style={styles.impotant}>Важность задачи:</Text>
                    <Pressable onPress={() => setModalVisible(true)}>
                        <Text style={styles.button}>{impotant}</Text>
                    </Pressable>
                </View>
                <ExecutionTime
                    toggle={toggle}
                    setToggle={setToggle}
                    setDate={setDate}>
                    {date}
                </ExecutionTime>
                <View>
                    <View style={styles.timeButtons}>
                        <Pressable onPress={handleReturn}>
                            <Text style={styles.button}>отменить</Text>
                        </Pressable>
                        <Pressable onPress={handleSubmit}>
                            <Text style={styles.button}>сохранить</Text>
                        </Pressable>
                    </View>
                </View>
            </ScrollView>
        </View>
    );
};

export default CreateScreen;

import React, {useState} from 'react';
import {View, FlatList, Button} from 'react-native';
import {useSelector} from 'react-redux';
import TodoItem from './TodoItem';
import ImpotantModal from './ImpotantModal';
import {styles} from './styles';

const TodoList = ({navigation}) => {
    const {todo} = useSelector(store => store.todo);
    const [modalVisible, setModalVisible] = useState(false);
    const [impFilter, setImpFilter] = useState('все');

    const handleImpotant = value => {
        setImpFilter(value);
        setModalVisible(false);
    };

    const renderItem = ({item}) =>
        (impFilter === 'все' || item.impotant === impFilter) && (
            <TodoItem
                item={item}
                navigation={navigation}
                keyExtractor={obj => obj.id}
            />
        );

    return (
        <View style={styles.wrapper}>
            <ImpotantModal
                setImpotant={handleImpotant}
                setVisible={setModalVisible}
                visible={modalVisible}>
                {['все', 'обычная', 'важная', 'очень важная']}
            </ImpotantModal>
            <View style={styles.btnWrapper}>
                <Button
                    title={impFilter}
                    onPress={() => setModalVisible(true)}
                />
            </View>
            <FlatList
                data={todo}
                renderItem={renderItem}
                keyExtractor={item => item.id}
            />
        </View>
    );
};

export default TodoList;

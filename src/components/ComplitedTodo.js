import React from 'react';
import {ScrollView, Text, View, Pressable} from 'react-native';
import {useDispatch} from 'react-redux';
import {todoActions} from '../store/todo/actions';
import {styles} from './styles';

const ComplitedTodo = ({navigation, item}) => {
    const dispatch = useDispatch();

    const handleDelete = () => {
        dispatch(
            todoActions.deleteTodo({
                id: item.id,
            }),
        );
        navigation.goBack();
    };

    const handleReturn = () => {
        navigation.goBack();
    };

    return (
        <ScrollView>
            <Text style={styles.input}>{item.title}</Text>
            <Text style={styles.input}>{item.description}</Text>
            <View>
                <Text style={styles.impotant}>Важность задачи:</Text>
                <Text style={styles.button}>{item.impotant}</Text>
            </View>
            <View style={styles.timeWrap}>
                <View style={styles.input}>
                    <Text>Время запланированное:</Text>
                    <Text>
                        {item.useDate
                            ? new Date(item.date).toLocaleString()
                            : 'Не задано.'}
                    </Text>
                </View>
                <View style={styles.input}>
                    <Text>Время фактическое:</Text>
                    <Text>
                        {new Date(item.completionTime).toLocaleString()}
                    </Text>
                </View>
            </View>
            <View style={styles.timeButtons}>
                <Pressable onPress={handleReturn}>
                    <Text style={styles.button}>отменить</Text>
                </Pressable>
                <Pressable onPress={handleDelete}>
                    <Text style={styles.button}>удалить</Text>
                </Pressable>
            </View>
        </ScrollView>
    );
};

export default ComplitedTodo;

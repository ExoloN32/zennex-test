import React, {useEffect} from 'react';
import {Button, View, AppState} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {useSelector, useDispatch} from 'react-redux';
import {todoActions} from '../store/todo/actions';
import TodoList from './TodoList';
import {styles} from './styles';

let td = null;

const HomeScreen = ({navigation}) => {
    const dispatch = useDispatch();
    const {todo} = useSelector(store => store.todo);

    useEffect(() => {
        td = todo;
    }, [todo]);

    useEffect(() => {
        AppState.addEventListener('change', handleAppStateChange);
        retrieveData();
        return () => {
            AppState.removeEventListener('change', handleAppStateChange);
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const handleAppStateChange = nextAppState => {
        if (nextAppState === 'background') {
            storeData();
        }
    };

    const storeData = async () => {
        try {
            let json = JSON.stringify(td);
            await AsyncStorage.setItem('TASKS', json);
        } catch (error) {
            console.log('save error', error);
        }
    };

    const retrieveData = async () => {
        try {
            let value = await AsyncStorage.getItem('TASKS');
            if (value !== null) {
                value = await JSON.parse(value);
                dispatch(todoActions.updateAllTodo(value));
            }
        } catch (error) {
            console.log('loading error', error);
        }
    };

    return (
        <View style={styles.container}>
            <View style={styles.btnWrapper}>
                <Button
                    title="Создать задачу"
                    onPress={() => navigation.navigate('Create')}
                />
            </View>
            <TodoList navigation={navigation} />
        </View>
    );
};

export default HomeScreen;

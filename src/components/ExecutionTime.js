import React, {useState} from 'react';
import {View, Text, Pressable, Switch, Platform} from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';
import {styles} from './styles';

const ExecutionTime = ({toggle, setToggle, setDate, children}) => {
    const [show, setShow] = useState(false);
    const [mode, setMode] = useState('date');

    const onChange = (event, selectedDate) => {
        const currentDate = selectedDate;
        setShow(Platform.OS === 'ios');
        setDate(currentDate);
    };

    const showMode = currentMode => {
        setShow(true);
        setMode(currentMode);
    };

    const showDatepicker = () => {
        showMode('date');
    };

    const showTimepicker = () => {
        showMode('time');
    };

    return (
        <View style={styles.timeWrap}>
            <View style={styles.exec}>
                <Text>Время исполнения:</Text>
                <Switch
                    trackColor={{false: '#767577', true: '#81b0ff'}}
                    thumbColor={toggle ? '#f5dd4b' : '#f4f3f4'}
                    ios_backgroundColor="#3e3e3e"
                    onValueChange={setToggle}
                    value={toggle}
                />
            </View>
            {toggle && (
                <View>
                    <Text>{children.toLocaleString()}</Text>
                    <View style={styles.timeButtons}>
                        <Pressable onPress={showDatepicker}>
                            <Text style={styles.button}>дата</Text>
                        </Pressable>
                        <Pressable onPress={showTimepicker}>
                            <Text style={styles.button}>время</Text>
                        </Pressable>
                    </View>
                </View>
            )}
            {show && (
                <DateTimePicker
                    testID="dateTimePicker"
                    value={children}
                    mode={mode}
                    is24Hour={true}
                    display="default"
                    onChange={onChange}
                />
            )}
        </View>
    );
};

export default ExecutionTime;

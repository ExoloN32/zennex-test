import React, {useState} from 'react';
import {ScrollView, TextInput, View, Text, Pressable} from 'react-native';
import {useDispatch} from 'react-redux';
import {todoActions} from '../store/todo/actions';
import ExecutionTime from './ExecutionTime';
import ImpotantModal from './ImpotantModal';
import {styles} from './styles';
import {titleAlert} from '../utils/titleAlert';
import {timeAlert} from '../utils/timeAlert';

const EditTodo = ({navigation, item}) => {
    const dispatch = useDispatch();
    const [title, setTitle] = useState(item.title);
    const [description, setDescription] = useState(item.description);
    const [impotant, setImpotant] = useState(item.impotant);
    const [modalVisible, setModalVisible] = useState(false);
    const [toggle, setToggle] = useState(item.useDate);
    const [date, setDate] = useState(new Date(Number(item.date)));

    const handleImpotant = value => {
        setImpotant(value);
        setModalVisible(false);
    };

    const handleReturn = () => {
        navigation.goBack();
    };

    const handleSubmit = () => {
        if (title.trim().length < 1) {
            titleAlert();
            return;
        }
        if (toggle && new Date().getTime() > new Date(date).getTime()) {
            timeAlert();
            return;
        }
        dispatch(
            todoActions.changeTodo({
                id: item.id,
                title,
                description,
                impotant,
                useDate: toggle,
                date: date.getTime(),
                complited: false,
                completionTime: new Date().getTime(),
            }),
        );
        navigation.goBack();
    };

    const handleComplete = () => {
        dispatch(
            todoActions.changeTodo({
                id: item.id,
                title: item.title,
                description: item.description,
                impotant: item.impotant,
                useDate: item.useDate,
                date: item.date,
                complited: true,
                completionTime: new Date().getTime(),
            }),
        );
        navigation.goBack();
    };

    const handleDelete = () => {
        dispatch(
            todoActions.deleteTodo({
                id: item.id,
            }),
        );
        navigation.goBack();
    };

    return (
        <View>
            <ImpotantModal
                setImpotant={handleImpotant}
                setVisible={setModalVisible}
                visible={modalVisible}>
                {['обычная', 'важная', 'очень важная']}
            </ImpotantModal>
            <ScrollView>
                <TextInput
                    style={styles.input}
                    onChangeText={setTitle}
                    value={title}
                    placeholder="Название"
                    maxLength={50}
                />
                <TextInput
                    style={styles.input}
                    onChangeText={setDescription}
                    value={description}
                    placeholder="Описание"
                    multiline
                />
                <View>
                    <Text style={styles.impotant}>Важность задачи:</Text>
                    <Pressable onPress={() => setModalVisible(true)}>
                        <Text style={styles.button}>{impotant}</Text>
                    </Pressable>
                </View>
                <ExecutionTime
                    toggle={toggle}
                    setToggle={setToggle}
                    setDate={setDate}>
                    {date}
                </ExecutionTime>
                <View>
                    <Pressable onPress={handleComplete}>
                        <Text style={styles.button}>завершить задачу</Text>
                    </Pressable>
                    <View style={styles.timeButtons}>
                        <Pressable onPress={handleReturn}>
                            <Text style={styles.button}>отменить</Text>
                        </Pressable>
                        <Pressable onPress={handleSubmit}>
                            <Text style={styles.button}>изменить</Text>
                        </Pressable>
                    </View>
                    <Pressable onPress={handleDelete}>
                        <Text style={styles.button}>удалить</Text>
                    </Pressable>
                </View>
            </ScrollView>
        </View>
    );
};

export default EditTodo;

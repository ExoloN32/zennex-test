import React from 'react';
import {View} from 'react-native';
import ComplitedTodo from './ComplitedTodo';
import EditTodo from './EditTodo';
import {styles} from './styles';

const TodoScreen = ({navigation, route}) => {
    const complited = route.params.item.complited;

    return (
        <View style={styles.container}>
            {complited ? (
                <ComplitedTodo
                    navigation={navigation}
                    item={route.params.item}
                />
            ) : (
                <EditTodo navigation={navigation} item={route.params.item} />
            )}
        </View>
    );
};

export default TodoScreen;

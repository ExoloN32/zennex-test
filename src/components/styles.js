import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 10,
    },
    wrapper: {
        flex: 1,
        marginTop: 10,
    },
    btnWrapper: {
        marginVertical: 5,
    },
    input: {
        marginVertical: 10,
        borderColor: '#777',
        borderWidth: 1,
        borderRadius: 5,
        paddingHorizontal: 5,
    },
    button: {
        textAlign: 'center',
        backgroundColor: '#9f9',
        marginVertical: 10,
        padding: 10,
        fontSize: 20,
        borderRadius: 5,
        minWidth: '45%',
    },
    timeButtons: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    timeWrap: {
        borderColor: '#777',
        borderWidth: 1,
        borderRadius: 5,
        paddingHorizontal: 10,
        paddingBottom: 10,
    },
    exec: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: 15,
    },
    modal: {
        borderColor: '#777',
        borderWidth: 1,
        borderRadius: 5,
        backgroundColor: '#ffffff',
        padding: 10,
    },
    modalText: {
        textAlign: 'center',
        margin: 5,
        backgroundColor: '#aaffdd',
        borderRadius: 5,
        padding: 10,
        fontSize: 20,
    },
    centeredView: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#00000099',
    },
    red: {
        borderColor: 'red',
    },
    yellow: {
        borderColor: '#dd0',
    },
});

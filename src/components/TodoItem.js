import React from 'react';
import {View, Text, Pressable} from 'react-native';
import {styles} from './styles';

const TodoItem = ({navigation, item}) => {
    return (
        <View
            style={[
                styles.input,
                item.useDate && item.date < new Date().getTime() && styles.red,
                item.complited && styles.yellow,
            ]}>
            <Pressable
                onPress={() => navigation.navigate('Todo', {item: {...item}})}>
                <Text>{item.title}</Text>
                <Text>
                    {item.useDate
                        ? new Date(item.date).toLocaleString()
                        : 'Время не установленно.'}
                </Text>
                <Text>{item.impotant}</Text>
            </Pressable>
        </View>
    );
};

export default TodoItem;

import React from 'react';
import {View, Text, Pressable, Modal} from 'react-native';
import {styles} from './styles';

const ImpotantModal = ({setImpotant, visible, setVisible, children}) => {
    return (
        <Modal
            animationType="fade"
            transparent={true}
            visible={visible}
            onRequestClose={() => {
                setVisible(!visible);
            }}>
            <View style={styles.centeredView}>
                <View style={styles.modal}>
                    <Text>Важность задачи:</Text>
                    {React.Children.map(children, child => (
                        <Pressable onPress={() => setImpotant(child)}>
                            <Text style={styles.modalText}>{child}</Text>
                        </Pressable>
                    ))}
                </View>
            </View>
        </Modal>
    );
};

export default ImpotantModal;

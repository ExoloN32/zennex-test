import {todoActionTypes} from './actions';

const initialState = {
    todo: [],
};

export const todoReducer = (state = initialState, action) => {
    switch (action.type) {
        case todoActionTypes.ADD_TODO:
            return {
                ...state,
                todo: [...state.todo, action.payload],
            };

        case todoActionTypes.DELETE_TODO:
            return {
                ...state,
                todo: state.todo.filter(item => {
                    return item.id !== action.payload.id;
                }),
            };

        case todoActionTypes.CHANGE_TODO:
            return {
                ...state,
                todo: state.todo.map(item => {
                    if (item.id === action.payload.id) {
                        return action.payload;
                    } else {
                        return item;
                    }
                }),
            };
        case todoActionTypes.UPDATE_ALL_TODO:
            return {
                ...state,
                todo: action.payload,
            };

        default:
            return state;
    }
};

export const todoActionTypes = {
    ADD_TODO: 'TODO.ADD_TODO',
    CHANGE_TODO: 'TODO.CHANGE_TODO',
    DELETE_TODO: 'TODO.DELETE_TODO',
    UPDATE_ALL_TODO: 'TODO.UPDATE_ALL_TODO',
};

export const todoActions = {
    addTodo: payload => ({type: todoActionTypes.ADD_TODO, payload}),
    changeTodo: payload => ({type: todoActionTypes.CHANGE_TODO, payload}),
    deleteTodo: payload => ({type: todoActionTypes.DELETE_TODO, payload}),
    updateAllTodo: payload => ({
        type: todoActionTypes.UPDATE_ALL_TODO,
        payload,
    }),
};

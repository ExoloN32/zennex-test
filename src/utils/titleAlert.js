import {Alert} from 'react-native';

export const titleAlert = () =>
    Alert.alert(
        'Ошибка',
        'Поле заголовок не должно быть пустым.',
        [
            {
                text: 'OK',
                style: 'cancel',
            },
        ],
        {
            cancelable: true,
        },
    );

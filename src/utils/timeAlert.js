import {Alert} from 'react-native';

export const timeAlert = () =>
    Alert.alert(
        'Ошибка',
        'Время исполнения задачи должно быть больше текущего.',
        [
            {
                text: 'OK',
                style: 'cancel',
            },
        ],
        {
            cancelable: true,
        },
    );

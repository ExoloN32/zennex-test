import 'react-native-gesture-handler';
import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {Provider} from 'react-redux';
import {store} from './src/store';
import HomeScreen from './src/components/HomeScreen';
import CreateScreen from './src/components/CreateScreen';
import TodoScreen from './src/components/TodoScreen';

const Stack = createStackNavigator();

const App = () => {
    return (
        <Provider store={store}>
            <NavigationContainer>
                <Stack.Navigator initialRouteName="Home">
                    <Stack.Screen
                        name="Home"
                        component={HomeScreen}
                        options={{title: 'Список задач'}}
                    />
                    <Stack.Screen
                        name="Create"
                        component={CreateScreen}
                        options={{title: 'Создание задачи'}}
                    />
                    <Stack.Screen
                        name="Todo"
                        component={TodoScreen}
                        options={{title: 'Редактирование задачи'}}
                    />
                </Stack.Navigator>
            </NavigationContainer>
        </Provider>
    );
};

export default App;
